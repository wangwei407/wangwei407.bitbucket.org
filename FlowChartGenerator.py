'''
Author:Wei

Test Code for dnaseq.py first 5 steps

I assume my class receive a list functions(step) name(type:string) from dnaseq

Then my class create a flowchart which describes the specific steps and save it in local

'''
# import pydot python module which is a binding to graphviz(written by c)
import pydot
#registry for step function   key:name of function value:function object
#e.g: key:picard_sam_to_fastq value:function object
registry={}
def register(step_function):
    '''
    :param function: a function object
    :add the function to the registry
    '''
    registry[step_function.__name__]=step_function
    return step_function

class FlowChartGenerator:
    '''
    a flowchart generator based on the input(selected stpes) of user
    '''
    def __init__(self,pipeline_type,arrange_direction='LR'):
        '''
        :param pipeline_type: name for specific class (e.g dnaseq),which will output on bottom of the graph
        :param arrange_direction: LR means from left to right,TB means from Top to bottom
        '''
        self._graph=pydot.Dot(label=pipeline_type,rankDir=arrange_direction)
        self._registered_functions=registry
        self._arrange_direction=arrange_direction
    @property
    def graph(self):
        return self._graph
    @property
    def registered_functions(self):
        return self._registered_functions
    def __call__(self, steps):
        return self._check_steps(steps)
    def _check_steps(self,steps):
        '''
        :param steps: a step list which user input
        '''
        node_list=[]
        #check steps and find the
        for step_name in steps:
            for function_name,function in self._registered_functions.items():
                #print (function_name,function)
                if step_name==function_name:
                    step_node=function(self)
                    node_list.append(step_node)
        #connect nodes
        self.connecter(node_list)
        #arranege step nodes if the direction is LR
        if self._arrange_direction=='LR':
            self.arrange_step_nodes(node_list)
        #Finally draw the graph
        self._graph.write_png('graph.png')
    def generate_graph(self):
        pass
    @register
    def picard_sam_to_fastq(self):
        '''
        step1:picard_sam_to_fastq => generate fastq files from bam files(input: BAM/SAM files)
        '''
        step_name='(picard_sam_to_fastq)'
        action_name='Generate FASTQ files from files'
        input_name='SAM/BAM files'
        step_node=self.generate_step_node(step_name,action_name,not_input_node=False,input_node_name=input_name)
        return step_node
    @register
    def trimmomatic(self):
        '''
        step2:trimmomatic => clean fastq files (input: FASTQ files)
        '''
        step_name='(trimmomatic)'
        action_name='Clean FASTQ files'
        input_name='FASTQ files'
        step_node=self.generate_step_node(step_name,action_name,not_input_node=False,input_node_name=input_name)
        return step_node
    @register
    def merge_trimmomatic_stats(self):
        '''
        step3: merge_trimmomatic_stats => merge individuals cleaning metrics
        '''
        step_name='(merge_trimmomatic_stats)'
        action_name='merge individuals cleaning metrics'
        step_node=self.generate_step_node(step_name,action_name)
        return step_node
    @register
    def bwa_mem_picard_sort_files(self):
        '''
        step4:bwa_mem_picard_sort_sam => generate bam files from fastq files
        '''
        step_name='(bwa_mem_picard_sort_sam)'
        action_name='Generate BAM files from FASTQ files'
        step_node=self.generate_step_node(step_name,action_name)
        return step_node
    @register
    def picard_merge_sam_files(self):
        '''
        step5:picard_merge_sam_files => merge individuals bam files (input: BAM files)
        '''
        step_name='(bwa_mem_picard_sort_files)'
        action_name='Merge individuals bam files'
        input_name='BAM files'
        step_node=self.generate_step_node(step_name,action_name,not_input_node=False,input_node_name=input_name)
        return step_node
    def connecter(self,node_list):
        '''
        connect nodes based on node_list and direction (e.g. LR or BT)
        :param node_list: a list which contain nodes
        '''
        for index in range(len(node_list)-1):
            #connect index and index+1 node
            self.generate_edge_between_nodes(node_list[index][0],node_list[index+1][0])
        if self._arrange_direction=='BT':
            for step_node in node_list:
                self._graph.add_subgraph(step_node[1])
    def arrange_step_nodes(self,nodes_list):
        subgraph=pydot.Subgraph(rank='same')
        for node in nodes_list:
            subgraph.add_node(node[0])
        self._graph.add_subgraph(subgraph)

    def generate_step_node(self,step_name,action,not_input_node=True,input_node_name='',):
        '''
        :param step_name: The node print information such as step num,function name(e.g step1,picard_sam_to_fastq)
        :param action: The node print action information. e.g node(step1) will has a action: generate fastq files from bam files
        :param input: default is false(means this step don't have input)
        :return: The (step,subgraph) tuple which is convenient for connection, if direction is 'LR', subgraph=None
        '''
        subgraph=None
        step_node=self.generate_start_end_node(step_name)
        action_node=self.generate_action_node(action)
        #step_node->action_node
        edge_step_action=self.generate_edge_between_nodes(step_node,action_node)
        if not_input_node:
            if self._arrange_direction=='LR':
                pass
            else:
                subgraph=pydot.Subgraph(rank='same')
                subgraph.add_node(step_node)
                subgraph.add_node(action_node)
        else:
            input_node=self.generate_input_output_node(input_node_name)
            #input_node->step_node
            edge__input_step=self.generate_edge_between_nodes(input_node,step_node)
            if self._arrange_direction=='LR':
                pass
            else:
                subgraph=pydot.Subgraph(rank='same')
                subgraph.add_node(step_node)
                subgraph.add_node(action_node)
                subgraph.add_node(input_node)
        return (step_node,subgraph)

    def generate_start_end_node(self,name):
        '''
        generate an oval shape node for start/end point,then add it to the self.graph
        :param name: name of node
        :return: reference of generated node
        '''
        start_end_node=pydot.Node(name=name,
                          fontcolor='navy',
                          color='darkgoldenrod2',
                          shape='oval',
                          style='filled')
        self._graph.add_node(start_end_node)
        return start_end_node
    def generate_input_output_node(self,name):
        '''
        generate a parallelogram shape node for input/output node
        '''
        input_output_node=pydot.Node(name,
                          fontcolor='navy',
                          color='grey80',
                          shape='parallelogram',
                          style='filled')
        self._graph.add_node(input_output_node)
        return input_output_node
    def generate_action_node(self,name):
        '''
        generate a rect shape node for action node
        '''
        action_node= pydot.Node(name=name,
                          fontcolor='white',
                          color='turquoise',
                          shape='rect',
                          style='filled')
        self._graph.add_node(action_node)
        return action_node
    def generate_edge_between_nodes(self,nodea,nodeb,label=''):
        '''
        :param: nodea,nodeb connect a and b
        :param: label for edge
        '''
        edge_between_nodes=pydot.Edge(nodea,nodeb,label=label)
        self._graph.add_edge(edge_between_nodes)
        return edge_between_nodes
if __name__ == '__main__':
    #test
    functions_list=['picard_sam_to_fastq',
                   
                    ]
    chart=FlowChartGenerator('dnaseq','BT')
    chart(functions_list)
