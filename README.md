# README #

* Author: wei
* Email: wangwei407@wustl.edu

### What is this repository for? ###

* Quick summary
  :A flowchart engine to create flowchart file based on dnaseq pipeline
* Params: FlowChartGenerator(pipeline_name,direction)
I make my generator class callable, user can both draw flowchart like this:
* generator=FlowChartGenerator(pipeline_name,direction,step_list)
   generator.draw()
* generator=FlowChartGenerator(pipeline_name,direction)
   generator(step_list)
# Output 
##My generator can generate flowchart based on the different steps. (e.g 4 steps and 1 step) 

### One step![graph1step.png](https://bitbucket.org/repo/ednyrB/images/1395763487-graph1step.png)

### Four steps![graph4steps.png](https://bitbucket.org/repo/ednyrB/images/83048626-graph4steps.png)
##My generator can arrange flowchart direction.
### From left to right![graphLR.png](https://bitbucket.org/repo/ednyrB/images/2244791249-graphLR.png)
### From top to bottom![graphBT.png](https://bitbucket.org/repo/ednyrB/images/2516527256-graphBT.png)